# storage/ssd/nvme_xfs_50G_file_create_bz1227342

Storage: Format nvme to xfs and create a 50G file 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You need run test_dev_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
